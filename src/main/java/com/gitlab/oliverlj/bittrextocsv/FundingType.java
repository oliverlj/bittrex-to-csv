package com.gitlab.oliverlj.bittrextocsv;

public enum FundingType {

  DEPOSIT, WITHDRAWAL;

}
