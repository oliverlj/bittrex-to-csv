package com.gitlab.oliverlj.bittrextocsv;

import java.math.BigDecimal;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import lombok.Data;

@Data
@JsonPropertyOrder({"id", "date", "type", "currency", "quantity", "cost", "cryptoAddress", "txId"})
public class Funding {

  private String id;

  private FundingType type;

  private BigDecimal quantity;

  private BigDecimal cost;

  private String currency;

  private Date date;

  private String txId;

  private String cryptoAddress;

}
