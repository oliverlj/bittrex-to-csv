package com.gitlab.oliverlj.bittrextocsv;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UncheckedIOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.knowm.xchange.ExchangeSpecification;
import org.knowm.xchange.bittrex.BittrexExchange;
import org.knowm.xchange.bittrex.dto.account.BittrexDepositHistory;
import org.knowm.xchange.bittrex.dto.account.BittrexWithdrawalHistory;
import org.knowm.xchange.bittrex.service.BittrexAccountService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.csv.CsvGenerator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;

@SpringBootApplication
public class BittrexToCsvApplication {

  public static void main(String[] args) throws IOException {
    ConfigurableApplicationContext run = SpringApplication.run(BittrexToCsvApplication.class, args);
    fundingsToCsv("put-here-your-api-key", "put-here-your-secret-key");
    run.stop();
  }

  private static void fundingsToCsv(String apiKey, String secretKey) throws IOException {
    BittrexExchange exchange = new BittrexExchange();

    ExchangeSpecification exSpec = exchange.getDefaultExchangeSpecification();
    exSpec.setApiKey(apiKey);
    exSpec.setSecretKey(secretKey);
    exSpec.setShouldLoadRemoteMetaData(false);
    exchange.applySpecification(exSpec);

    List<Funding> fundings = fetchFundings(exchange, FundingType.DEPOSIT);
    fundings.addAll(fetchFundings(exchange, FundingType.WITHDRAWAL));

    writeCsv(fundings);
  }

  private static List<Funding> fetchFundings(BittrexExchange exchange, FundingType type) throws IOException {
    BittrexAccountService accountService = (BittrexAccountService) exchange.getAccountService();

    List<Funding> fundings = new ArrayList<>();

    List<Funding> fundingsFromBittrex = new ArrayList<>();
    do {
      String token = fundingsFromBittrex.isEmpty() ? "" : fundingsFromBittrex.get(fundingsFromBittrex.size() - 1).getId();
      fundingsFromBittrex = fetchFundingsFromBittrex(type, accountService, token);
      fundings.addAll(fundingsFromBittrex);
    } while (!fundingsFromBittrex.isEmpty());

    return fundings;
  }

  private static List<Funding> fetchFundingsFromBittrex(FundingType type, BittrexAccountService accountService, String token) {
    try {
      if (type == FundingType.DEPOSIT) {
        List<BittrexDepositHistory> deposits = accountService.getBittrexDepositsClosed(null, token, "", 200);
        return deposits.stream().map(BittrexToCsvApplication::toFunding).collect(Collectors.toList());
      } else {
        List<BittrexWithdrawalHistory> withdrawals;
        withdrawals = accountService.getBittrexWithdrawalsClosed(null, token, "", 200);
        return withdrawals.stream().map(BittrexToCsvApplication::toFunding).collect(Collectors.toList());
      }
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }

  private static Funding toFunding(BittrexDepositHistory deposit) {
    Funding funding = new Funding();

    funding.setId(deposit.getId());
    funding.setType(FundingType.DEPOSIT);
    funding.setDate(deposit.getUpdatedAt());
    funding.setCurrency(deposit.getCurrencySymbol());
    funding.setQuantity(deposit.getQuantity());
    funding.setCryptoAddress(deposit.getCryptoAddress());
    funding.setTxId(deposit.getTxId());
    return funding;
  }

  private static Funding toFunding(BittrexWithdrawalHistory withdrawal) {
    Funding funding = new Funding();

    funding.setId(withdrawal.getId());
    funding.setType(FundingType.WITHDRAWAL);
    funding.setDate(withdrawal.getCompletedAt());
    funding.setCurrency(withdrawal.getCurrencySymbol());
    funding.setQuantity(withdrawal.getQuantity());
    funding.setCost(withdrawal.getTxCost());
    funding.setCryptoAddress(withdrawal.getCryptoAddress());
    funding.setTxId(withdrawal.getTxId());
    return funding;
  }

  private static void writeCsv(List<Funding> fundings) {
    // create mapper and schema
    CsvMapper mapper = new CsvMapper();
    mapper.enable(CsvGenerator.Feature.ALWAYS_QUOTE_STRINGS);
    CsvSchema schema = mapper.schemaFor(Funding.class).withUseHeader(true);
    // schema = schema.withColumnSeparator('\t');

    // output writer
    ObjectWriter myObjectWriter = mapper.writer(schema);
    File tempFile = new File("fundings.csv");
    try {
      FileOutputStream tempFileOutputStream;
      tempFileOutputStream = new FileOutputStream(tempFile);
      BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(tempFileOutputStream, 1024);
      OutputStreamWriter writerOutputStream = new OutputStreamWriter(bufferedOutputStream, "UTF-8");
      myObjectWriter.writeValue(writerOutputStream, fundings);
    } catch (IOException e) {
      throw new UncheckedIOException(e);
    }
  }
}
